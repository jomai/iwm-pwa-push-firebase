### How to setup a PWA and how to send notifications via Firebase?

If you are hosting your Website on Firebase, its really easy (or easier)
1. Create your Firebase Account
2. Create a Project
3. Create a App in the Project
4. Download the Firebase Tools for CLI
5. Create a local Firebase Project via the CLI <br>
5.1 If you are not using Firebase Hosting, be sure to run npm install with all your dependencies for firebase
6. You now have the public directory. Fill it with whatever you like, but keep an eye on the Scripts that will be loaded. <br>
6.1 In some Cases you might have to reorder the Imports in the index html for your project
7. For PWA Support create a main.js in the dir js/ and a manifest.json. Be sure to include both in your index.html <br>
7.1 As we are going to register the service worker in the main.js, I advice you to load it **after** the imports of firebase, so everything is up and running, when your service worker tries to handle messages or whatever <br>
7.2 If you are not using Firebase Hosting, be sure to manully init Firebase and Messaging also - rewrite my imports in the other files. Because they rely on the firebase modules to be accessable.
8. Create a ServiceWorker in the public root. In case of Firebase Messaging the name has to be firebase-messaging-sw.js (sw stands for Service Worker)
9. Let the Service Worker handle the background notifications.
10. Go to your Google Developer Profile, create a Projekt and Register that for the FCN API. I don't think you ever need that key anywhere, but it is used to generate tokens, i guess. Please fact-check that.
11. Go to your Firebase Console and go to Cloud Messaging and Start your way there.
12. In your Index Html you should add the request for notification permission and other handler, please see for yourself. As I tried to relocate those to my main.js or Service Worker, it broke. So please make sure, that your handlers are where they should be or need to be.
12. Go to your CLI and type firebase deploy.
13. Visit your Page and accept the notifications. You should see, when using my project, that you have no a Token (sometimes you need to reload the page. idk why)
14. Google firebase notification composer. Create your Notification and send it to your app.
15. Be happy and Enjoy your on pwa with push notifications.